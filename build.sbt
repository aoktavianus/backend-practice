name := "practice_1"

version := "0.1"

scalaVersion := "2.12.6"


lazy val anak = (project in file("anak"))
  .enablePlugins(LagomScala)
  .settings(
    name := "anak",
    scalaVersion := "2.12.6",
    libraryDependencies ++= Seq(
      evolutions,
      lagomScaladslTestKit,
      lagomScaladslPersistenceJdbc,
      "com.typesafe.play" %% "play-slick" % "3.0.0",
      "com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",
      "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided",
      "org.scalatest" %% "scalatest" % "3.0.3" % Test,
      "org.mockito" % "mockito-core" % "2.10.0" % "test",
      "com.github.dnvriend" %% "akka-persistence-jdbc" % "3.4.0",
      "org.postgresql" % "postgresql" % "42.2.5"
    )
  )
  .dependsOn(orangtua)

lazy val orangtua = (project in file("orangtua"))
