addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.15")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.5")


addSbtPlugin("com.lightbend.lagom" % "lagom-sbt-plugin" % "1.4.8")