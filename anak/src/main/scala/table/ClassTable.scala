package table

import model.Class
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

/**
  * Created by alvinoktavianus (https://id.linkedin.com/in/alvinoktavianus)
  * on 14/09/18
  */

final class ClassTable(tag: Tag) extends Table[Class](tag, "classes") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def className = column[String]("class_name", O.Length(255))

  def courseId = column[Long]("course_id")

  def userId = column[Long]("user_id")

  override def * = (id, className, courseId, userId).mapTo[Class]

  // Foreign Key and Unique definition

  lazy val courses = TableQuery[CourseTable]
  lazy val users = TableQuery[UserTable]

  def course = foreignKey("classes_course_id_fkey", courseId, courses)(_.id)

  def user = foreignKey("classes_user_id_fkey", userId, users)(_.id)

  def classNameCourseIdUserIdUnique = index("classes_class_name_course_id_user_id_key", (className, courseId, userId), unique = true)

}
