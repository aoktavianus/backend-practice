package table

import model.Course
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

final class CourseTable(tag: Tag) extends Table[Course](tag, "courses") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def code = column[String]("code", O.Length(10))

  override def * = (id, code).mapTo[Course]

}
