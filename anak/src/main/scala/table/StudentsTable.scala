package table

import model.Student
import slick.jdbc.PostgresProfile.api._

class StudentsTable(tag: Tag) extends Table[Student](tag, "students") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def name = column[String]("full_name")

  def classId = column[Int]("class_id")

  override def * = (id, name, classId) <> ((Student.apply _).tupled, Student.unapply)

}
