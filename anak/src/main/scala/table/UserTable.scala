package table

import model.User
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

final class UserTable(tag: Tag) extends Table[User](tag, "users") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def fullName = column[String]("full_name", O.Length(100))

  def userType = column[String]("type", O.Length(10))

  def email = column[String]("email", O.Length(100), O.Unique)

  def phoneNumber = column[String]("phone_number", O.Length(15))

  override def * = (id, fullName, userType, email, phoneNumber).mapTo[User]

}
