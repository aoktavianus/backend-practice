package service

import model.Student
import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.transport.Method._
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import response._
import Service._
import request.{UpdatedUserRequest, UserRequest}

trait AnakService extends Service {

  def viewStudents(id_class: Int): ServiceCall[NotUsed, AnakResponses]

  def addNewStudent(): ServiceCall[Student, AnakResponse]

  def getAllUsers(userType: Option[String]): ServiceCall[NotUsed, Seq[UserResponse]]

  def postNewUser(): ServiceCall[UserRequest, UserResponse]

  def getUserById(id: Long): ServiceCall[NotUsed, UserResponse]

  def updateUserById(id: Long): ServiceCall[UpdatedUserRequest, UserResponse]

  def getAllCourse: ServiceCall[NotUsed, Seq[CourseResponse]]

  def getCourseById(id: Long): ServiceCall[NotUsed, CourseResponse]

  def getAllClass: ServiceCall[NotUsed, Seq[ClassResponse]]

  override def descriptor: Descriptor = {

    named("anak").withCalls(
      // user API
      restCall(PUT, "/users/:id", updateUserById _),
      restCall(POST, "/users", postNewUser _),
      restCall(GET, "/users/:id", getUserById _),
      restCall(GET, "/users?user_type", getAllUsers _),

      // course API
      restCall(GET, "/courses/:id", getCourseById _),
      restCall(GET, "/courses", getAllCourse _),

      // class API
      restCall(GET, "/classes", getAllClass _),

      // student API
      restCall(GET, "/students/:class_id", viewStudents _),
      restCall(POST, "/students", addNewStudent _)
    )
  }

}
