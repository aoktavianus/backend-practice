package service

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.transport.BadRequest
import com.lightbend.lagom.scaladsl.server
import com.lightbend.lagom.scaladsl.server.ServerServiceCall
import model.{Student, User}
import repositories.{ClassRepository, CourseRepository, UserRepository}
import request.{UpdatedUserRequest, UserRequest}
import response._

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}


class AnakServiceImpl(studentDataService: StudentDataService,
                      userRepository: UserRepository,
                      courseRepository: CourseRepository,
                      classRepository: ClassRepository)
                     (implicit ec: ExecutionContext) extends AnakService {

  override def viewStudents(class_id: Int): ServiceCall[NotUsed, AnakResponses] = ServerServiceCall.apply { _ =>
    studentDataService.getStudents(class_id, 1, 10).map(
      students =>
        AnakResponses(
          students.map(student =>
            AnakResponse(student.id, student.name, student.classId)
          )
        )
    )
  }

  override def addNewStudent: ServiceCall[Student, AnakResponse] = server.ServerServiceCall.apply { request =>
    studentDataService.insertStudent(request).map {
      case Success(anak) =>
        AnakResponse(anak.id, anak.name, anak.classId)
      case Failure(exception) =>
        throw BadRequest(exception.getMessage)
    }
  }

  override def getAllUsers(userType: Option[String]): ServiceCall[NotUsed, Seq[UserResponse]] = ServerServiceCall.apply { _ =>
    userType match {
      case Some(value) => userRepository.findAllByUserTypeIgnoreCase(value).map(
        users =>
          users.map(user =>
            UserResponse(user.id, user.fullName, user.userType, user.email, user.phoneNumber)
          )
      )
      case None => userRepository.findAll.map(
        users =>
          users.map(user =>
            UserResponse(user.id, user.fullName, user.userType, user.email, user.phoneNumber)
          )
      )
    }
  }

  override def postNewUser(): ServiceCall[UserRequest, UserResponse] = ServerServiceCall.apply { request =>
    // TODO: Find a better way to ignore insertion of id
    val newUser = User(0l, request.fullName, request.userType, request.email, request.phoneNumber)
    userRepository.save(newUser).map {
      user =>
        UserResponse(user.id, user.fullName, user.userType, user.email, user.phoneNumber)
    }
  }

  override def getUserById(id: Long): ServiceCall[NotUsed, UserResponse] = ServerServiceCall.apply { _ =>
    userRepository.findById(id).map {
      value =>
        UserResponse(value.id, value.fullName, value.userType, value.email, value.phoneNumber)
    }
  }

  override def updateUserById(id: Long): ServiceCall[UpdatedUserRequest, UserResponse] = ServerServiceCall.apply { request =>
    // TODO: Find a better way to ignore insertion of id
    val updatedUser = User(0l, request.fullName, "", "", request.phoneNumber)
    userRepository.update(id, updatedUser).map {
      user =>
        UserResponse(user.id, user.fullName, user.userType, user.email, user.phoneNumber)
    }
  }

  override def getAllCourse: ServiceCall[NotUsed, Seq[CourseResponse]] = ServerServiceCall.apply { _ =>
    courseRepository.findAll.map {
      courses =>
        courses.map {
          course =>
            CourseResponse(course.id, course.code)
        }
    }
  }

  override def getCourseById(id: Long): ServiceCall[NotUsed, CourseResponse] = ServerServiceCall.apply { _ =>
    courseRepository.findById(id).map {
      course =>
        CourseResponse(course.id, course.code)
    }
  }

  override def getAllClass: ServiceCall[NotUsed, Seq[ClassResponse]] = ServerServiceCall.apply { _ =>
    classRepository.findAll.map {
      classes =>
        classes.map {
          cl =>
            ClassResponse(cl.id, cl.className, cl.courseId, cl.userId)
        }
    }
  }

}