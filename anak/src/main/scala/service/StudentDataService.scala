package service

import model.Student
import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile.api._
import _root_.table.StudentsTable
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}


class StudentDataService(databaseConfig: DatabaseConfig[JdbcProfile]) {

  val studentQuery = TableQuery[StudentsTable]
  val db = databaseConfig.db

  def getStudents(classId: Int, page: Int, perPage: Int): Future[Seq[Student]] = {
    val drop = (page - 1) * perPage
    val query = studentQuery
      .filter(_.classId === classId)
      .drop(drop)
      .take(perPage)
      .result
    query.statements.foreach(println)
    db.run(query)
  }

  def insertStudent(student: Student)(implicit ec: ExecutionContext): Future[Try[Student]] = {
    val query = studentQuery returning studentQuery.map(_.id) into ((student, id) => student.copy(id = id)) += student
    db.run(query).map(
      result => Success(result)
    ).recover {
      case ex =>
        Failure(ex)
    }
  }
}
