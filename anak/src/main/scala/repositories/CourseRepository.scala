package repositories

import model.Course
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import table.CourseTable

import scala.concurrent.Future

class CourseRepository(databaseConfig: DatabaseConfig[JdbcProfile]) {

  val courses = TableQuery[CourseTable]
  val db = databaseConfig.db

  def findAll: Future[Seq[Course]] = db.run(courses.result)

  def findById(id: Long): Future[Course] = db.run(courses.filter(_.id === id).take(1).result.head)

  def findAllById(id: Long): Future[Seq[Course]] = db.run(courses.filter(_.id === id).result)

}
