package repositories

import model.Class
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import table.{ClassTable, CourseTable, UserTable}

import scala.concurrent.Future

/**
  * Created by alvinoktavianus (https://id.linkedin.com/in/alvinoktavianus)
  * on 14/09/18
  */

class ClassRepository(databaseConfig: DatabaseConfig[JdbcProfile]) {

  val db = databaseConfig.db
  val classes = TableQuery[ClassTable]
  val courses = TableQuery[CourseTable]
  val users = TableQuery[UserTable]

  def findAll: Future[Seq[Class]] = db.run(classes.result)

  def findById(id: Long): Future[Class] = db.run(classes.filter(_.id === id).take(1).result.head)

}
