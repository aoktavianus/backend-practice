package repositories

import model.User
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import table.UserTable

import scala.concurrent.Future

class UserRepository(databaseConfig: DatabaseConfig[JdbcProfile]) {

  val userQuery = TableQuery[UserTable]
  val db = databaseConfig.db

  def findAll: Future[Seq[User]] = db.run(userQuery.result)

  def findAllByUserTypeIgnoreCase(userType: String): Future[Seq[User]] = db.run(userQuery.filter(_.userType === userType.toUpperCase()).result)

  def save(user: User): Future[User] = {
    val q = userQuery returning userQuery.map(_.id) into ((user, id) => user.copy(id = id)) += user
    db.run(q)
  }

  def findById(id: Long): Future[User] = db.run(userQuery.filter(_.id === id).take(1).result.head)

  def update(id: Long, user: User): Future[User] = {
    val q = userQuery.filter(_.id === id).map(user => (user.fullName, user.phoneNumber))
    db.run(q.update(user.fullName, user.phoneNumber))
    findById(id)
  }

}
