package response

import play.api.libs.json.JsonNaming.SnakeCase
import play.api.libs.json.{Format, Json, JsonConfiguration}

case class UserResponse(id: Long,
                        fullName: String,
                        userType: String,
                        email: String,
                        phoneNumber: String)

object UserResponse {
  implicit val config = JsonConfiguration(SnakeCase)
  implicit val format: Format[UserResponse] = Json.format
}
