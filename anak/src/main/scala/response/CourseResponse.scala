package response

import play.api.libs.json.JsonNaming.SnakeCase
import play.api.libs.json.{Format, Json, JsonConfiguration}

case class CourseResponse(id: Long,
                          name: String)

object CourseResponse {
  implicit val config = JsonConfiguration(SnakeCase)
  implicit val format: Format[CourseResponse] = Json.format
}
