package response

import play.api.libs.json.JsonNaming.SnakeCase
import play.api.libs.json.{Format, Json, JsonConfiguration}

/**
  * Created by alvinoktavianus (https://id.linkedin.com/in/alvinoktavianus)
  * on 14/09/18
  */

case class ClassResponse(id: Long,
                         className: String,
                         courseId: Long,
                         userId: Long)

object ClassResponse {
  implicit val config = JsonConfiguration(SnakeCase)
  implicit val format: Format[ClassResponse] = Json.format
}
