package response

import play.api.libs.json.{Format, Json}

case class AnakResponse(id: Int,
                        full_name: String,
                        class_id: Int)

case class AnakResponses(students: Seq[AnakResponse])

object AnakResponse {

  implicit val format: Format[AnakResponse] = Json.format

}

object AnakResponses {

  implicit val format: Format[AnakResponses] = Json.format

}
