package request

import play.api.libs.json.JsonNaming.SnakeCase
import play.api.libs.json.{Format, Json, JsonConfiguration}

final case class UserRequest(email: String,
                             fullName: String,
                             userType: String,
                             phoneNumber: String)

object UserRequest {
  implicit val config = JsonConfiguration(SnakeCase)
  implicit val format: Format[UserRequest] = Json.format
}
