package request

import play.api.libs.json.JsonNaming.SnakeCase
import play.api.libs.json.{Format, Json, JsonConfiguration}

final case class UpdatedUserRequest(fullName: String,
                                    phoneNumber: String)

object UpdatedUserRequest {
  implicit val config = JsonConfiguration(SnakeCase)
  implicit val format: Format[UpdatedUserRequest] = Json.format
}
