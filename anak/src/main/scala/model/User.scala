package model

final case class User(id: Long,
                      fullName: String,
                      userType: String,
                      email: String,
                      phoneNumber: String)
