package model

final case class Course(id: Long,
                        code: String)
