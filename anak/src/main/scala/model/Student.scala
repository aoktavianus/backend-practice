package model

import play.api.libs.json.{Format, Json}

case class Student(id: Int, name: String, classId: Int)

object Student {
  implicit val format: Format[Student] = Json.format
}