package model

/**
  * Created by alvinoktavianus (https://id.linkedin.com/in/alvinoktavianus)
  * on 14/09/18
  */

final case class Class(id: Long,
                       className: String,
                       courseId: Long,
                       userId: Long)
