package loader

import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomServer}
import play.api.db.HikariCPComponents
import play.api.db.evolutions.EvolutionsComponents
import play.api.db.slick.evolutions.SlickDBApi
import play.api.db.slick.{DbName, SlickComponents}
import play.api.libs.ws.ahc.AhcWSComponents
import repositories.{ClassRepository, CourseRepository, UserRepository}
import service.{AnakService, AnakServiceImpl, StudentDataService}
import slick.jdbc.JdbcProfile


abstract class AnakApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
    with HikariCPComponents
    with EvolutionsComponents
    with SlickComponents {

  applicationEvolutions

  lazy val dbApi = SlickDBApi(slickApi)

  lazy val dbConfigProvider = slickApi.dbConfig[JdbcProfile](DbName.apply("default"))

  val studentDataService = new StudentDataService(dbConfigProvider)
  val userRepository = new UserRepository(dbConfigProvider)
  val courseRepository = new CourseRepository(dbConfigProvider)
  val classRepository = new ClassRepository(dbConfigProvider)

  val anakService = new AnakServiceImpl(studentDataService, userRepository, courseRepository, classRepository)

  override def lagomServer: LagomServer = serverFor[AnakService](new AnakServiceImpl(studentDataService, userRepository, courseRepository, classRepository))

}
