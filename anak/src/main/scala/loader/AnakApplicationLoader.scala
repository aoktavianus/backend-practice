package loader

import com.lightbend.lagom.scaladsl.client.ConfigurationServiceLocatorComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader}

class AnakApplicationLoader extends LagomApplicationLoader {
  override def load(context: LagomApplicationContext): LagomApplication = {
    new AnakApplication(context) with ConfigurationServiceLocatorComponents
  }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication = {
    new AnakApplication(context) with LagomDevModeComponents
  }
}