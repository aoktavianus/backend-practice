# --- !Ups

create domain unsigned_integer as integer constraint unsigned_integer_check check (value >= 0 and value <= 2147483647);

create domain gender as char constraint proper_gender check (value in ('M', 'F'));

create domain user_type as varchar(10) constraint proper_user_type check (value in ('STUDENT', 'LECTURE'));

create domain email_address as varchar(100) constraint proper_email check (value ~*
                                                                           '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$');

create domain phone_number as varchar(15) constraint proper_phone_number check ((value ~* '^[0-9]+$') and
                                                                                (length(value) >= 5));

create table users (
  id           serial        not null primary key,
  full_name    varchar(100)  not null,
  type         user_type     not null,
  email        email_address not null unique,
  phone_number phone_number  not null unique,
  created_at   timestamp with time zone default current_timestamp,
  updated_at   timestamp with time zone default current_timestamp
);

create table courses (
  id         serial      not null primary key,
  code       varchar(10) not null unique,
  created_at timestamp with time zone default current_timestamp,
  updated_at timestamp with time zone default current_timestamp
);

create table classes (
  id         serial           not null primary key,
  class_name varchar(255)     not null,
  course_id  unsigned_integer not null references courses (id),
  user_id    unsigned_integer not null references users (id),
  created_at timestamp with time zone default current_timestamp,
  updated_at timestamp with time zone default current_timestamp,
  unique (class_name, course_id, user_id)
);

create or replace function trigger_set_updated_at_timestamp()
  returns trigger as $$
begin
  new.updated_at = current_timestamp;;
  return new;;
end;;
$$
language plpgsql;

create trigger set_updated_at_timestamp_courses
  before update
  on courses
  for each row
execute procedure trigger_set_updated_at_timestamp();

create trigger set_updated_at_timestamp_classes
  before update
  on classes
  for each row
execute procedure trigger_set_updated_at_timestamp();

create trigger set_updated_at_timestamp_users
  before update
  on users
  for each row
execute procedure trigger_set_updated_at_timestamp();

# --- !Downs

drop table classes;
drop table courses;
drop table users;
drop function trigger_set_updated_at_timestamp();
drop domain phone_number;
drop domain unsigned_integer;
drop domain gender;
drop domain user_type;
drop domain email_address;
